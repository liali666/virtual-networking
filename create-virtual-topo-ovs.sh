#!/bin/sh
COMMON_DIR=$(dirname $(readlink -f $BASH_SOURCE))
. $COMMON_DIR/include.sh

#  Topo:
#
#  +-----------------------------------------------------------------------+
#  |  RHEL                                                                 |
#  |                                                                       |
#  |                                                                       |
#  |                                                                       |
#  |  +-------+  +-------+  +-------+    +-------+  +-------+  +-------+   |
#  |  | netns |  | netns |  | netns |    | netns |  | netns |  | netns |   |
#  |  |vlan1-1|  |vlan1-2|  |vlan1-3|    |vlan3-1|  |vlan3-2|  |vlan3-3|   | 
#  |  +--+----+  +---+---+  +---+---+    +---+---+  +---+---+  +---+---+   |
#  |     |           |          |            |          |          |       |
#  |     |           |          |            |          |          |       |
#  |     |           |          |            |          |          |       |
#  |     |           |          |            |          |          |       |
#  |  +--+-----------+----------+------------+----------+----------+---+   |
#  |  |                                                                |   |
#  |  |    ovs(br0)                                                    |   |
#  |  |                                                                |   |
#  |  +---------------------------+------------------------------------+   |
#  |                              |                                        |
#  |                              |                                        |
#  |                              |                                        |
#  |                              |                                        |
#  |  +---------------------------+------------------------------------+   |
#  |  |                                                                |   |
#  |  |    bond0 mode=2 xmit_hash_policy=vlan+srcmac tlb_dynamic_lb=0  |   |
#  |  |                                                                |   |
#  |  +--+----------------------------------------------------------+--+   |
#  |     |link1                                                link2|      |
#  |     |                                                          |      |
#  |     |                                                          |      |
#  |     |                                                          |      |
#  |  +--+-----------------+                      +-----------------+--+   |
#  |  |                    |                      |                    |   |
#  |  | sw1                +----------------------+                sw2 |   |
#  |  +---------+----------+                      +-----------------+--+   |
#  |            |                                                   |      |
#  |            |                                                   |      |
#  |            |                                                   |      |
#  |            |link3                                        link4 |      |
#  |  +---------+---------------------------------------------------+--+   |
#  |  |                                                                |   |
#  |  |    bond1 mode=2 xmit_hash_policy=vlan+srcmac tlb_dynamic_lb=0  |   |
#  |  |                                                                |   |
#  |  +------------------------------------+---------------------------+   |
#  |                                       |                               |
#  |                                       |                               |
#  |                                       |                               |
#  |                                       |                               |
#  |  +------------------------------------+---------------------------+   |
#  |  |                                                                |   |
#  |  |    ovs(br4)                                                    |   |
#  |  |                                                                |   |
#  |  +--+-----------+----------------------------------+-----------+--+   |
#  |     |           |                                  |           |      | 
#  |     |           |                                  |           |      | 
#  |     |           |                                  |           |      | 
#  |     |           |                                  |           |      | 
#  |  +--+----+  +---+---+                          +---+---+  +----+--+   |
#  |  | netns |  | netns |                          | netns |  | netns |   |
#  |  |vlan1-4|  |vlan3-4|                          |vlan1-5|  |vlan3-5|   | 
#  |  +--+----+  +---+---+                          +---+---+  +---+---+   |
#  |                                                                       |
#  |                                                                       |
#  |                                                                       |
#  |                                                                       |
#  +-----------------------------------------------------------------------+

#set -x

get_rhel_MajorVersion()
{
	cat /etc/redhat-release | grep -E -o "[0-9]+.[0-9]+" | awk -F "." '{print $1}'
}

ovs_install()
{
	ovs-vsctl -V &>/dev/null && return 0

	local rhel_version=$(get_rhel_MajorVersion)
	
	if (($rhel_version <= 6)); then
		RPM_OVS=${RPM_OVS:-"http://download.devel.redhat.com/brewroot/packages/openvswitch/2.3.1/2.git20150113.el6/$(uname -m)/openvswitch-2.3.1-2.git20150113.el6.$(uname -m).rpm"}
	elif (($rhel_version == 7)); then
		RPM_OVS=${RPM_OVS:-"http://download.devel.redhat.com/brewroot/packages/openvswitch/2.7.0/8.git20170530.el7fdb/$(uname -m)/openvswitch-2.7.0-8.git20170530.el7fdb.$(uname -m).rpm"}
	elif (($rhel_version == 8)); then
		RPM_OVS=${RPM_OVS:-"http://download.eng.bos.redhat.com/brewroot/vol/rhel-8/packages/openvswitch2.17/2.17.0/64.el8fdp/$(uname -m)/openvswitch2.17-2.17.0-64.el8fdp.$(uname -m).rpm"}
	elif (($rhel_version == 9)); then
		RPM_OVS=${RPM_OVS:-"https://download.eng.bos.redhat.com/brewroot/vol/rhel-9/packages/openvswitch3.1/3.1.0/54.el9fdp/$(uname -m)/openvswitch3.1-3.1.0-54.el9fdp.$(uname -m).rpm"}
	else
		RPM_OVS=${RPM_OVS:-"https://download.eng.bos.redhat.com/brewroot/vol/rhel-9/packages/openvswitch3.1/3.1.0/54.el9fdp/$(uname -m)/openvswitch3.1-3.1.0-54.el9fdp.$(uname -m).rpm"}
	fi
	
	if (($rhel_version == 8)); then
		RPM_OVS_SELINUX_EXTRA_POLICY=${RPM_OVS_SELINUX_EXTRA_POLICY:-http://download-node-02.eng.bos.redhat.com/brewroot/packages/openvswitch-selinux-extra-policy/1.0/29.el8fdp/noarch/openvswitch-selinux-extra-policy-1.0-29.el8fdp.noarch.rpm}
	elif (($rhel_version == 9)); then
		RPM_OVS_SELINUX_EXTRA_POLICY=${RPM_OVS_SELINUX_EXTRA_POLICY:-http://download-node-02.eng.bos.redhat.com/brewroot/packages/openvswitch-selinux-extra-policy/1.0/34.el9fdp/noarch/openvswitch-selinux-extra-policy-1.0-34.el9fdp.noarch.rpm}
	else
		RPM_OVS_SELINUX_EXTRA_POLICY=${RPM_OVS_SELINUX_EXTRA_POLICY:-http://download.devel.redhat.com/brewroot/packages/openvswitch-selinux-extra-policy/1.0/11.el7fdp/noarch/openvswitch-selinux-extra-policy-1.0-11.el7fdp.noarch.rpm}
	fi
	
	
	if [ ! -e /usr/bin/python ];then
		install_python
		#ln -s /usr/bin/python3 /usr/bin/python || ln -s /usr/bin/python2 /usr/bin/python
	fi
	
	if (($rhel_version == 8)); then
		$YUM_INSTALL compat-openssl10
	fi

	# for rhel9.4
	rpm -q unbound-libs || dnf -y install unbound-libs
	
	$YUM_INSTALL $RPM_OVS_SELINUX_EXTRA_POLICY
	$YUM_INSTALL $RPM_OVS
}

function cleanup_topo()
{
        if test "${TEST_SETUP}" == "nft"; then
                nft 'delete table netdev nt0'
                nft 'delete table netdev nt1'
        elif test "${TEST_SETUP}" == "nft_nm"; then
                nmcli con del sw1-bond0-veth0
                nmcli con del sw2-bond0-veth0
                nmcli con del bond0
                nmcli con del sw1-bond1-veth0
                nmcli con del sw2-bond1-veth0
                nmcli con del bond1
		ovs-vsctl del-br br0
		ovs-vsctl del-br br4
		nmcli con del br0 &>/dev/null
		nmcli con del br4 &>/dev/null
        elif test "${TEST_SETUP}" == "bpf"; then
                pkt-loop-filter --unload bond0
                pkt-loop-filter --unload bond1
        fi
	echo $-|grep -q e && e_enabled=yes || e_enabled=no
	set +e
	ip link del br0 &>/dev/null
	ip link del br4 &>/dev/null
	ip link del sw1
	ip link del sw2
	modprobe -rv bonding
	ip netns del vlan1-1
	ip netns del vlan1-2
	ip netns del vlan1-3
	ip netns del vlan1-4
	ip netns del vlan1-5
	ip netns del vlan3-1
	ip netns del vlan3-2
	ip netns del vlan3-3
	ip netns del vlan3-4
	ip netns del vlan3-5
	ip link del vlan1-1-veth0 &>/dev/null
	ip link del vlan1-2-veth0 &>/dev/null
	ip link del vlan1-3-veth0 &>/dev/null
	ip link del vlan1-4-veth0 &>/dev/null
	ip link del vlan1-5-veth0 &>/dev/null
	ip link del vlan3-1-veth0 &>/dev/null
	ip link del vlan3-2-veth0 &>/dev/null
	ip link del vlan3-3-veth0 &>/dev/null
	ip link del vlan3-4-veth0 &>/dev/null
	ip link del vlan3-5-veth0 &>/dev/null
        ip link del sw1-bond0-veth0 &>/dev/null 
        ip link del sw2-bond0-veth0 &>/dev/null
        ip link del sw1-bond1-veth0 &>/dev/null
        ip link del sw2-bond1-veth0 &>/dev/null
	ip link del sw1-sw2-veth0 &>/dev/null
	ip link del vlan1-1-veth1 &>/dev/null
	ip link del vlan1-2-veth1 &>/dev/null
	ip link del vlan1-3-veth1 &>/dev/null
	ip link del vlan1-4-veth1 &>/dev/null
	ip link del vlan1-5-veth1 &>/dev/null
	ip link del vlan3-1-veth1 &>/dev/null
	ip link del vlan3-2-veth1 &>/dev/null
	ip link del vlan3-3-veth1 &>/dev/null
	ip link del vlan3-4-veth1 &>/dev/null
	ip link del vlan3-5-veth1 &>/dev/null
        ip link del sw1-bond0-veth1 &>/dev/null 
        ip link del sw2-bond0-veth1 &>/dev/null
        ip link del sw1-bond1-veth1 &>/dev/null
        ip link del sw2-bond1-veth1 &>/dev/null
	ip link del sw1-sw2-veth0 &>/dev/null
	ip link del sw1-sw2-veth1 &>/dev/null
	systemctl stop openvswitch
	[ "$e_enabled" == "yes" ] && set -e
}

# define mac and ip
ipaddr=252
mac1=$(printf 20:00:00:00:%02x:98 $ipaddr)
mac2=$(printf 22:00:00:00:%02x:99 $ipaddr)
mac3=$(printf 22:22:22:21:%02x:22 $ipaddr)
mac4=$(printf 20:00:00:00:%02x:98 $ipaddr)
mac5=$(printf 26:00:00:00:%02x:99 $ipaddr)
mac6=$(printf 24:22:22:21:%02x:22 $ipaddr)
mac7=$(printf 2a:22:22:21:%02x:20 $ipaddr)
mac8=$(printf 2a:22:22:21:%02x:21 $ipaddr)
mac9=$(printf 2a:22:22:21:%02x:22 $ipaddr)
mac10=$(printf 2a:22:22:21:%02x:23 $ipaddr)
ip1="172.22.$ipaddr.2"
ip2="172.22.$ipaddr.3"
ip3="172.22.$ipaddr.4"
ip4="172.23.$ipaddr.2"
ip5="172.23.$ipaddr.3"
ip6="172.23.$ipaddr.4"
ip7="172.22.$ipaddr.5"
ip8="172.22.$ipaddr.6"
ip9="172.23.$ipaddr.5"
ip10="172.23.$ipaddr.6"
ip6_1="2009:22::$ipaddr:2"
ip6_2="2009:22::$ipaddr:3"
ip6_3="2009:22::$ipaddr:4"
ip6_4="2009:23::$ipaddr:2"
ip6_5="2009:23::$ipaddr:3"
ip6_6="2009:23::$ipaddr:4"
ip6_7="2009:22::$ipaddr:5"
ip6_8="2009:22::$ipaddr:6"
ip6_9="2009:23::$ipaddr:5"
ip6_10="2009:23::$ipaddr:6"

vlan1_1_veth1_ofport=11
vlan1_2_veth1_ofport=12
vlan1_3_veth1_ofport=13
vlan1_4_veth0_ofport=14
vlan1_5_veth0_ofport=15
vlan3_1_veth1_ofport=21
vlan3_2_veth1_ofport=22
vlan3_3_veth1_ofport=23
vlan3_4_veth0_ofport=24
vlan3_5_veth0_ofport=25
bond0_ofport=26
bond1_ofport=27
		

function do_nft()
{
        echo $@
        nft $@
}

function setup_nft()
{
        local table=$1
        local bond=$2

        do_nft "add table netdev ${table}"
        do_nft "add chain netdev ${table} ${bond}EgressFilter { type filter hook egress device ${bond} priority 0; }"
        do_nft "add chain netdev ${table} ${bond}IngressFilter { type filter hook ingress device ${bond} priority 0; }"
        do_nft "add set netdev ${table} macset { type ether_addr; flags timeout; }"
        do_nft "add rule netdev ${table} ${bond}EgressFilter set update ether saddr timeout 5s @macset"
        do_nft "add rule netdev ${table} ${bond}IngressFilter ether saddr @macset counter drop"
}

function setup_bpf()
{
        local bond=$1
        shift;

        pkt-loop-filter $bond
}

function setup_topo(){
	
	cleanup_topo &>/dev/null || { echo "Waring when cleanup_topo."; }
	ovs_install
	systemctl start openvswitch

	 # create bridge 
        if test "${TEST_SETUP}" == "nft_nm"; then
		#nmcli connection add con-name br4 type bridge ifname br4 bridge.vlan-filtering yes ipv4.method manual ipv4.address 199.19.19.19/24 ipv6.method manual ipv6.address 2019:19::19/64 bridge.stp no bridge.multicast-snooping yes bridge.multicast-querier yes bridge.ageing-time 600
		#nmcli connection up br4
		#nmcli connection add con-name br0 type bridge ifname br0 bridge.vlan-filtering yes ipv4.method manual ipv4.address 199.19.19.20/24 ipv6.method manual ipv6.address 2019:19::20/64 bridge.stp no bridge.multicast-snooping yes bridge.multicast-querier no bridge.ageing-time 600
		#nmcli connection up br0
		ovs-vsctl add-br br0
		ovs-vsctl add-br br4
		ip link set br0 up
		ip link set br4 up
        else
		ip link add name br0 type bridge vlan_filtering 1
		echo 1 > /sys/class/net/br0/bridge/multicast_snooping
		echo 0 > /sys/class/net/br0/bridge/multicast_querier
		ip link set br0 up
		ip link add name br4 type bridge vlan_filtering 1
		echo 1 > /sys/class/net/br4/bridge/multicast_snooping
		echo 1 > /sys/class/net/br4/bridge/multicast_querier
		ip link set br4 up
        fi

	# create sw1,sw2
	ip link add name sw1 type bridge vlan_filtering 1
	echo 0 > /sys/class/net/sw1/bridge/multicast_snooping
	echo 0 > /sys/class/net/sw1/bridge/multicast_querier
	echo 60000 > /sys/class/net/sw1/bridge/ageing_time
	ip link set sw1 up
	ip link add name sw2 type bridge vlan_filtering 1
	echo 0 > /sys/class/net/sw2/bridge/multicast_snooping
	echo 0 > /sys/class/net/sw2/bridge/multicast_querier
	echo 60000 > /sys/class/net/sw2/bridge/ageing_time
	ip link set sw2 up
	#echo 3 > /sys/class/net/br0/bridge/multicast_igmp_version
	#echo 3 > /sys/class/net/br4/bridge/multicast_igmp_version
	echo 3 > /sys/class/net/sw1/bridge/multicast_igmp_version
	echo 3 > /sys/class/net/sw2/bridge/multicast_igmp_version
	#echo 1 > /proc/sys/net/ipv6/conf/br0/keep_addr_on_down
	#echo 1 > /proc/sys/net/ipv6/conf/br4/keep_addr_on_down
	echo 1 > /proc/sys/net/ipv6/conf/sw1/keep_addr_on_down
	echo 1 > /proc/sys/net/ipv6/conf/sw2/keep_addr_on_down
	ip addr add 199.19.19.21/24 dev sw1
	ip addr add 2019:19::21/64 dev sw1
	ip addr add 199.19.19.22/24 dev sw2
	ip addr add 2019:19::22/64 dev sw2
	
	# create netns and veths
	ip netns add vlan1-1
	ip netns add vlan1-2
	ip netns add vlan1-3
	ip netns add vlan1-4
	ip netns add vlan1-5
	ip netns add vlan3-1
	ip netns add vlan3-2
	ip netns add vlan3-3
	ip netns add vlan3-4
	ip netns add vlan3-5
	
	ip link add name vlan1-1-veth0 type veth peer name vlan1-1-veth1
	ip link add name vlan1-2-veth0 type veth peer name vlan1-2-veth1
	ip link add name vlan1-3-veth0 type veth peer name vlan1-3-veth1
	ip link add name vlan1-4-veth0 type veth peer name vlan1-4-veth1
	ip link add name vlan1-5-veth0 type veth peer name vlan1-5-veth1
	ip link add name vlan3-1-veth0 type veth peer name vlan3-1-veth1
	ip link add name vlan3-2-veth0 type veth peer name vlan3-2-veth1
	ip link add name vlan3-3-veth0 type veth peer name vlan3-3-veth1
	ip link add name vlan3-4-veth0 type veth peer name vlan3-4-veth1
	ip link add name vlan3-5-veth0 type veth peer name vlan3-5-veth1
	ip link add name sw1-bond0-veth0 type veth peer name sw1-bond0-veth1
	ip link add name sw2-bond0-veth0 type veth peer name sw2-bond0-veth1
	ip link add name sw1-bond1-veth0 type veth peer name sw1-bond1-veth1
	ip link add name sw2-bond1-veth0 type veth peer name sw2-bond1-veth1

	# connect netns vlan1-1 and br0
	ip link set vlan1-1-veth0 netns vlan1-1 up
	ip link set vlan1-1-veth1 up
	ovs-vsctl add-port br0 vlan1-1-veth1 -- set Interface vlan1-1-veth1 ofport=$vlan1_1_veth1_ofport
	
	# connect netns vlan1-2 and br0
	ip link set vlan1-2-veth0 netns vlan1-2 up
	ip link set vlan1-2-veth1 up
	ovs-vsctl add-port br0 vlan1-2-veth1 -- set Interface vlan1-2-veth1 ofport=$vlan1_2_veth1_ofport
	
	# connect netns vlan1-3 and br0
	ip link set vlan1-3-veth0 netns vlan1-3 up
	ip link set vlan1-3-veth1 up
	ovs-vsctl add-port br0 vlan1-3-veth1 -- set Interface vlan1-3-veth1 ofport=$vlan1_3_veth1_ofport
	
	# connect netns vlan1-4 and br4
	ip link set vlan1-4-veth1 netns vlan1-4 up
	ip link set vlan1-4-veth0 up
	ovs-vsctl add-port br4 vlan1-4-veth0 -- set Interface vlan1-4-veth0 ofport=$vlan1_4_veth0_ofport
	
	# connect netns vlan1-5 and br4
	ip link set vlan1-5-veth1 netns vlan1-5 up
	ip link set vlan1-5-veth0 up
	ovs-vsctl add-port br4 vlan1-5-veth0 -- set Interface vlan1-5-veth0 ofport=$vlan1_5_veth0_ofport
	
	# connect netns vlan3-1 and br0
	ip link set vlan3-1-veth0 netns vlan3-1 up
	ip link set vlan3-1-veth1 up
	ovs-vsctl add-port br0 vlan3-1-veth1 -- set Interface vlan3-1-veth1 ofport=$vlan3_1_veth1_ofport
	
	# connect netns vlan3-2 and br0
	ip link set vlan3-2-veth0 netns vlan3-2 up
	ip link set vlan3-2-veth1 up
	ovs-vsctl add-port br0 vlan3-2-veth1 -- set Interface vlan3-2-veth1 ofport=$vlan3_2_veth1_ofport
	
	# connect netns vlan3-3 and br0
	ip link set vlan3-3-veth0 netns vlan3-3 up
	ip link set vlan3-3-veth1 up
	ovs-vsctl add-port br0 vlan3-3-veth1 -- set Interface vlan3-3-veth1 ofport=$vlan3_3_veth1_ofport
	
	# connect netns vlan3-4 and br4
	ip link set vlan3-4-veth1 netns vlan3-4 up
	ip link set vlan3-4-veth0 up
	ovs-vsctl add-port br4 vlan3-4-veth0 -- set Interface vlan3-4-veth0 ofport=$vlan3_4_veth0_ofport
	
	# connect netns vlan3-5 and br4
	ip link set vlan3-5-veth1 netns vlan3-5 up
	ip link set vlan3-5-veth0 up
	ovs-vsctl add-port br4 vlan3-5-veth0 -- set Interface vlan3-5-veth0 ofport=$vlan3_5_veth0_ofport
	
	
	# create bond device
        if test "${TEST_SETUP}" == "nft"; then
                ip link add dev bond0 type bond mode balance-xor miimon 100 \
                        xmit_hash_policy vlan+srcmac
                setup_nft "nt0" "bond0"
                ip link add dev bond1 type bond mode balance-xor miimon 100 \
                        xmit_hash_policy vlan+srcmac
                setup_nft "nt1" "bond1"
                ip link set bond0 up
                ip link set bond1 up
                ifenslave bond0 sw1-bond0-veth0 sw2-bond0-veth0
                ifenslave bond1 sw1-bond1-veth0 sw2-bond1-veth0
        elif test "${TEST_SETUP}" == "nft_nm"; then
		# copy dispatcher script
                #NM_DISPATCHERD="/etc/NetworkManager/dispatcher.d/"
                #REPO_DIR=$(dirname $(readlink -f $BASH_SOURCE))
                #REPO_DIR=${REPO_DIR%/}
                #\cp $REPO_DIR/mlag.sh "${NM_DISPATCHERD}"
                #chmod +x "${NM_DISPATCHERD}/mlag.sh"
                #trap "rm ${NM_DISPATCHERD}/mlag.sh" EXIT

		## bond-slb config
		#echo 'BOND_SLB_IFACES="bond0 bond1"' >  /etc/default/bond-slb
		#trap "rm /etc/default/bond-slb" EXIT

                nmcli connection add con-name bond0 type bond ifname bond0 bond.options "mode=balance-xor,balance-slb=1,xmit_hash_policy=vlan+srcmac" ipv4.method manual ipv4.address 199.19.19.23/24
                nmcli connection add con-name sw1-bond0-veth0 type ethernet ifname sw1-bond0-veth0 master bond0
                nmcli connection add con-name sw2-bond0-veth0 type ethernet ifname sw2-bond0-veth0 master bond0
                #
                nmcli connection add con-name bond1 type bond ifname bond1 bond.options "mode=balance-xor,balance-slb=1,xmit_hash_policy=vlan+srcmac" ipv4.method manual ipv4.address 199.19.19.24/24
                nmcli connection add con-name sw1-bond1-veth0 type ethernet ifname sw1-bond1-veth0 master bond1
                nmcli connection add con-name sw2-bond1-veth0 type ethernet ifname sw2-bond1-veth0 master bond1
                #
                nmcli connection up sw1-bond0-veth0
                nmcli connection up sw2-bond0-veth0
                nmcli connection up sw1-bond1-veth0
                nmcli connection up sw2-bond1-veth0
                nmcli connection up bond0
                nmcli connection up bond1
		ovs-vsctl add-port br0 bond0 -- set Interface bond0 ofport=$bond0_ofport
		ovs-vsctl add-port br4 bond1 -- set Interface bond1 ofport=$bond1_ofport
		ip addr add 199.19.19.19/24 dev br0
		ip addr add 199.19.19.20/24 dev br4
		ip addr add 2019:19::19/64 dev br0
		ip addr add 2019:19::20/64 dev br4
        elif test "${TEST_SETUP}" == "bpf"; then
                ip link add dev bond0 type bond mode balance-xor miimon 100 \
                        xmit_hash_policy vlan+srcmac
                ip link set bond0 up
                ifenslave bond0 sw1-bond0-veth0 sw2-bond0-veth0
                setup_bpf bond0 sw1-bond0-veth0 sw2-bond0-veth0
                ip link add dev bond1 type bond mode balance-xor miimon 100 \
                        xmit_hash_policy vlan+srcmac
                ip link set bond1 up
                ifenslave bond1 sw1-bond1-veth0 sw2-bond1-veth0
                setup_bpf bond1 sw1-bond1-veth0 sw2-bond1-veth0
        else
                ip link add dev bond0 type bond mode balance-xor miimon 100 \
                        xmit_hash_policy vlan+srcmac mac_filter 10
                ip link add dev bond1 type bond mode balance-xor miimon 100 \
                        xmit_hash_policy vlan+srcmac mac_filter 10
                ip link set bond0 up
                ip link set bond1 up
                ifenslave bond0 sw1-bond0-veth0 sw2-bond0-veth0
                ifenslave bond1 sw1-bond1-veth0 sw2-bond1-veth0
        fi

	#ip link set bond0 master br0
	#ip link set bond1 master br4
	
	# connect bond0 to sw1
	ip link set sw1-bond0-veth1 master sw1
	ip link set sw1-bond0-veth0 up
	ip link set sw1-bond0-veth1 up

	# connect bond1 to sw1
	ip link set sw1-bond1-veth1 master sw1
	ip link set sw1-bond1-veth0 up
	ip link set sw1-bond1-veth1 up
	
	# connect bond0 to sw2
	ip link set sw2-bond0-veth1 master sw2
	ip link set sw2-bond0-veth0 up
	ip link set sw2-bond0-veth1 up

	# connecto bond1 to sw2
	ip link set sw2-bond1-veth1 master sw2
	ip link set sw2-bond1-veth0 up
	ip link set sw2-bond1-veth1 up

	# connect sw1 and sw2
	ip link add name sw1-sw2-veth0 type veth peer name sw1-sw2-veth1
	ip link set sw1-sw2-veth0 master sw1
	ip link set sw1-sw2-veth1 master sw2
	ip link set sw1-sw2-veth0 up 
	ip link set sw1-sw2-veth1 up

	# change veth mac
	ip netns exec vlan1-1 ip link set vlan1-1-veth0 address $mac1 
	ip netns exec vlan1-2 ip link set vlan1-2-veth0 address $mac2
	ip netns exec vlan1-3 ip link set vlan1-3-veth0 address $mac3
	ip netns exec vlan3-1 ip link set vlan3-1-veth0 address $mac4
	ip netns exec vlan3-2 ip link set vlan3-2-veth0 address $mac5
	ip netns exec vlan3-3 ip link set vlan3-3-veth0 address $mac6
	ip netns exec vlan1-4 ip link set vlan1-4-veth1 address $mac7
	ip netns exec vlan1-5 ip link set vlan1-5-veth1 address $mac8
	ip netns exec vlan3-4 ip link set vlan3-4-veth1 address $mac9
	ip netns exec vlan3-5 ip link set vlan3-5-veth1 address $mac10

        # setup ip for netns
        ip netns exec vlan1-1 ip link set vlan1-1-veth0 up
	sleep 1
        ip netns exec vlan1-1 ip addr add $ip1/24 dev vlan1-1-veth0
	sleep 1
        ip netns exec vlan1-1 ip addr add $ip6_1/64 dev vlan1-1-veth0
	sleep 1
        ip netns exec vlan1-2 ip link set vlan1-2-veth0 up
	sleep 1
        ip netns exec vlan1-2 ip addr add $ip2/24 dev vlan1-2-veth0
	sleep 1
        ip netns exec vlan1-2 ip addr add $ip6_2/64 dev vlan1-2-veth0
	sleep 1
        ip netns exec vlan1-3 ip link set vlan1-3-veth0 up
	sleep 1
        ip netns exec vlan1-3 ip addr add $ip3/24 dev vlan1-3-veth0
	sleep 1
        ip netns exec vlan1-3 ip addr add $ip6_3/64 dev vlan1-3-veth0
	sleep 1
        ip netns exec vlan3-1 ip link set vlan3-1-veth0 up
	sleep 1
        ip netns exec vlan3-1 ip addr add $ip4/24 dev vlan3-1-veth0
	sleep 1
        ip netns exec vlan3-1 ip addr add $ip6_4/64 dev vlan3-1-veth0
	sleep 1
        ip netns exec vlan3-2 ip link set vlan3-2-veth0 up
	sleep 1
        ip netns exec vlan3-2 ip addr add $ip5/24 dev vlan3-2-veth0
	sleep 1
        ip netns exec vlan3-2 ip addr add $ip6_5/64 dev vlan3-2-veth0
	sleep 1
        ip netns exec vlan3-3 ip link set vlan3-3-veth0 up
	sleep 1
        ip netns exec vlan3-3 ip addr add $ip6/24 dev vlan3-3-veth0
	sleep 1
        ip netns exec vlan3-3 ip addr add $ip6_6/64 dev vlan3-3-veth0

        ip netns exec vlan1-4 ip link set vlan1-4-veth1 up
	sleep 1
        ip netns exec vlan1-4 ip addr add $ip7/24 dev vlan1-4-veth1
	sleep 1
        ip netns exec vlan1-4 ip addr add $ip6_7/64 dev vlan1-4-veth1
	sleep 1
        ip netns exec vlan1-5 ip link set vlan1-5-veth1 up
	sleep 1
        ip netns exec vlan1-5 ip addr add $ip8/24 dev vlan1-5-veth1
	sleep 1
        ip netns exec vlan1-5 ip addr add $ip6_8/64 dev vlan1-5-veth1
	sleep 1
        ip netns exec vlan3-4 ip link set vlan3-4-veth1 up
	sleep 1
        ip netns exec vlan3-4 ip addr add $ip9/24 dev vlan3-4-veth1
	sleep 1
        ip netns exec vlan3-4 ip addr add $ip6_9/64 dev vlan3-4-veth1
	sleep 1
        ip netns exec vlan3-5 ip link set vlan3-5-veth1 up
	sleep 1
        ip netns exec vlan3-5 ip addr add $ip10/24 dev vlan3-5-veth1
	sleep 1
        ip netns exec vlan3-5 ip addr add $ip6_10/64 dev vlan3-5-veth1

	# setup vlan
	ovs-vsctl set port bond0 vlan_mode=native-untagged tag=1 trunks=3
	ovs-vsctl set port bond1 vlan_mode=native-untagged tag=1 trunks=3
	ovs-vsctl set port vlan1-1-veth1 vlan_mode=access tag=1
	ovs-vsctl set port vlan1-2-veth1 vlan_mode=access tag=1
	ovs-vsctl set port vlan1-3-veth1 vlan_mode=access tag=1
	ovs-vsctl set port vlan3-1-veth1 vlan_mode=access tag=3
	ovs-vsctl set port vlan3-2-veth1 vlan_mode=access tag=3
	ovs-vsctl set port vlan3-3-veth1 vlan_mode=access tag=3
	
	bridge vlan add vid 3 dev sw1-bond0-veth1 
	bridge vlan add vid 3 dev sw1-bond1-veth1 
	bridge vlan add vid 3 dev sw2-bond0-veth1 
	bridge vlan add vid 3 dev sw2-bond1-veth1 
	bridge vlan add vid 3 dev sw1-sw2-veth0 
	bridge vlan add vid 3 dev sw1-sw2-veth1 
	
	ovs-vsctl set port vlan1-4-veth0 vlan_mode=access tag=1
	ovs-vsctl set port vlan1-5-veth0 vlan_mode=access tag=1
	ovs-vsctl set port vlan3-4-veth0 vlan_mode=access tag=3
	ovs-vsctl set port vlan3-5-veth0 vlan_mode=access tag=3

}

toggle_carrier()
{
	sleep 5
	# let nft create some entries first
        for i in vlan1-1%$ip6_7 vlan1-1%$ip6_8 vlan1-2%$ip6_7 vlan1-2%$ip6_8 vlan1-4%$ip6_1 vlan1-4%$ip6_2 vlan1-5%$ip6_1 vlan1-5%$ip6_2 vlan3-1%$ip6_9 vlan3-2%$ip6_10;do
                src=$(echo $i|awk -F"%" '{print $1}')
                dst=$(echo $i|awk -F"%" '{print $2}')
                # start ping
                echo "**** ping from $src to $dst ****" 
                ip netns exec $src ping6 -c5 $dst
        done
        for i in vlan1-1:$ip7 vlan1-1:$ip8 vlan1-2:$ip7 vlan1-2:$ip8 vlan1-4:$ip1 vlan1-4:$ip2 vlan1-5:$ip1 vlan1-5:$ip2 vlan3-1:$ip9 vlan3-2:$ip10;do
                src=$(echo $i|awk -F":" '{print $1}')
                dst=$(echo $i|awk -F":" '{print $2}')
                # start ping
                echo "**** ping from $src to $dst ****" 
                ip netns exec $src ping -c5 $dst
        done

	# toggle carrier quickly
	echo "******** toggle carrier quickly"
	for i in {1..500};do
	        ip link set sw1-bond0-veth1 down
	        ip link set sw1-bond0-veth1 up
	done
	for i in {1..500};do
	        if ((i%2==0));then
	                ip link set sw1-bond0-veth1 down
	                ip link set sw1-bond0-veth1 up
	        else
	                ip link set sw2-bond0-veth1 down
	                ip link set sw2-bond0-veth1 up
	        fi
	done
	echo "******** toggle carrier finish"
}

restart_networkmanager()
{
	sleep 5
	# let nft create some entries first
        for i in vlan1-1%$ip6_7 vlan1-1%$ip6_8 vlan1-2%$ip6_7 vlan1-2%$ip6_8 vlan1-4%$ip6_1 vlan1-4%$ip6_2 vlan1-5%$ip6_1 vlan1-5%$ip6_2 vlan3-1%$ip6_9 vlan3-2%$ip6_10;do
                src=$(echo $i|awk -F"%" '{print $1}')
                dst=$(echo $i|awk -F"%" '{print $2}')
                # start ping
                echo "**** ping from $src to $dst ****" 
                ip netns exec $src ping6 -c5 $dst 
        done
        for i in vlan1-1:$ip7 vlan1-1:$ip8 vlan1-2:$ip7 vlan1-2:$ip8 vlan1-4:$ip1 vlan1-4:$ip2 vlan1-5:$ip1 vlan1-5:$ip2 vlan3-1:$ip9 vlan3-2:$ip10;do
                src=$(echo $i|awk -F":" '{print $1}')
                dst=$(echo $i|awk -F":" '{print $2}')
                # start ping
                echo "**** ping from $src to $dst ****" 
                ip netns exec $src ping -c5 $dst  
        done

        echo "******** restart NM"
	systemctl restart NetworkManager
}

delete_connection()
{
	sleep 5
	# let nft create some entries first
        for i in vlan1-1%$ip6_7 vlan1-1%$ip6_8 vlan1-2%$ip6_7 vlan1-2%$ip6_8 vlan1-4%$ip6_1 vlan1-4%$ip6_2 vlan1-5%$ip6_1 vlan1-5%$ip6_2 vlan3-1%$ip6_9 vlan3-2%$ip6_10;do
                src=$(echo $i|awk -F"%" '{print $1}')
                dst=$(echo $i|awk -F"%" '{print $2}')
                # start ping
                echo "**** ping from $src to $dst ****" 
                ip netns exec $src ping6 -c5 $dst
        done
        for i in vlan1-1:$ip7 vlan1-1:$ip8 vlan1-2:$ip7 vlan1-2:$ip8 vlan1-4:$ip1 vlan1-4:$ip2 vlan1-5:$ip1 vlan1-5:$ip2 vlan3-1:$ip9 vlan3-2:$ip10;do
                src=$(echo $i|awk -F":" '{print $1}')
                dst=$(echo $i|awk -F":" '{print $2}')
                # start ping
                echo "**** ping from $src to $dst ****" 
                ip netns exec $src ping -c5 $dst
        done

        echo "******** recreate NM conn"
        #nmcli connection add con-name sw1-bond0-veth0 type ethernet ifname sw1-bond0-veth0 master bond0
        #nmcli connection add con-name sw2-bond0-veth0 type ethernet ifname sw2-bond0-veth0 master bond0
	nmcli con del sw1-bond0-veth0
	nmcli con del sw2-bond0-veth0
        nmcli connection add con-name sw1-bond0-veth0 type ethernet ifname sw1-bond0-veth0 master bond0
        nmcli connection add con-name sw2-bond0-veth0 type ethernet ifname sw2-bond0-veth0 master bond0
        ip link set bond0 down
        ip link set bond0 up
        nmcli con down bond0
        nmcli con up bond0
        #bridge vlan add vid 3 dev bond0
	ovs-vsctl set port bond0 vlan_mode=native-untagged tag=1 trunks=3
}

# in netns, disable port ipv6 accept_ra and setup link local address manually
netns_disable_ipv6_ra()
{
        local netns=$1
        local nic=$2
        local ipSegment1=$3
        local ipSegment2=$4
        local linkLocalAddr=$(printf "fe81::%02x:%02x" $ipSegment1 $ipSegment2)
	ip netns exec $netns bash <<- EOF
	ip link set $nic up
	echo 0 > /proc/sys/net/ipv6/conf/$nic/accept_ra
	echo 1 > /proc/sys/net/ipv6/conf/$nic/keep_addr_on_down
	sleep 1
	ip addr flush $nic
	ip addr add ${linkLocalAddr}/64 dev $nic
	EOF
}

setup_topo
