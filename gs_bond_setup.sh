P_I=100
P_J=10

bond_setup()
{ 
	
	local setup=$1;shift
	
	if [[ "$setup" == "bpf" ]];then
        	ip link add dev bond0 type bond mode balance-xor miimon 100 \
        	        xmit_hash_policy vlan+srcmac
        	ip link set bond0 up
        	ifenslave bond0 $@
        	pkt-loop-filter bond0
        	ip link add name br0 type bridge
        	ip link set br0 up
        	ip link set bond0 master br0
	elif [[ "$setup" == "nft" ]];then
		nmcli connection add con-name br0 type bridge ifname br0 bridge.vlan-filtering yes ipv4.method disable ipv6.method disable bridge.stp no bridge.ageing-time 9000
		nmcli connection up br0
		# swcfg setup_port_channel_without_lacp 93180 "Eth1/36 Eth1/35"
		#nmcli connection add con-name bond0 type bond ifname bond0 master br0 bond.options "mode=balance-xor,xmit_hash_policy=layer3+4"
		nmcli connection add con-name bond0 type bond ifname bond0 master br0 bond.options "mode=balance-xor,balance-slb=1,xmit_hash_policy=vlan+srcmac"

		for port in $@;do
			nmcli connection add con-name $port type ethernet ifname $port master bond0
		done

                nmcli connection up bond0
		for port in $@;do
			nmcli con up $port
		done
	else
        	#ip link add dev bond0 type bond mode 1 miimon 100 
        	ip link add dev bond0 type bond mode balance-xor miimon 100 xmit_hash_policy vlan+srcmac
        	ip link set bond0 up
        	ifenslave bond0 $@
        	ip link add name br0 type bridge
        	ip link set br0 up
        	ip link set bond0 master br0
	fi

        for i in $(seq 1 $P_I);do
        for j in $(seq 1 $P_J);do
                ip netns add ns${i}_${j}
                ip link add name ns${i}_${j}_veth0 type veth peer name ns${i}_${j}_veth1
                ip link set ns${i}_${j}_veth0 netns ns${i}_${j} up
                ip link set ns${i}_${j}_veth1 up
                ip link set ns${i}_${j}_veth1 master br0
                ip netns exec ns${i}_${j} ip link set ns${i}_${j}_veth0 up
                ip netns exec ns${i}_${j} ip link set ns${i}_${j}_veth0 address $(printf 42:%02x:00:00:03:%02x $i $j)
                ip netns exec ns${i}_${j} ip addr add 193.${i}.${j}.1/24 dev ns${i}_${j}_veth0
                ip netns exec ns${i}_${j} iperf3 -s -D
                #ip netns exec ns${i}_${j} netserver
	done
        done
}

bond_iperf3()
{
	local rtn=0
        for i in $(seq 1 $P_I);do
        for j in $(seq 1 $P_J);do
                ip netns exec ns${i}_${j} ping 193.${i}.${j}.2 -c1 || { rtn=1; echo "BUG: ip netns exec ns${i}_${j} ping 193.${i}.${j}.2 -c1 failed"; }
	done
        done

        #for i in $(seq 1 $P_I);do
        #for j in $(seq 1 $P_J);do
        for i in $(seq 1 1);do
        for j in $(seq 1 10);do
                ip netns exec ns${i}_${j} iperf3 -c 193.${i}.${j}.2 -f k -i 10 -t 180 | tee iperf_result${i}_${j} &
	done
        done
	wait
	total_rcv_rate=0
	total_snd_rate=0
        #for i in $(seq 1 $P_I);do
        #for j in $(seq 1 $P_J);do
        for i in $(seq 1 1);do
        for j in $(seq 1 10);do
		rcv_rate=$(cat iperf_result${i}_${j} | grep receiver | awk '{print $7}')
		snd_rate=$(cat iperf_result${i}_${j} | grep sender | awk '{print $7}')
		((total_rcv_rate+=rcv_rate))
		((total_snd_rate+=snd_rate))
	done
        done
	echo receiver: $total_rcv_rate
	echo sender: $total_snd_rate
	return $rtn
}

bond_cleanup()
{
	local setup=$1
	if [[ "$setup" == "bpf" ]];then
        	pkt-loop-filter --unload bond0
        	ip link del bond0
        	ip link del br0
        elif [[ "$setup" == "nft" ]];then
                nmcli connection del bond0
		for port in $(cat /sys/class/net/bond0/bonding/slaves);do
                        nmcli connection del $port
                done
		nmcli connection del br0
        	ip link del br0
		#rm -f /etc/NetworkManager/system-connections/bond0.nmconnection &>/dev/null
		#rm -f /etc/NetworkManager/system-connections/br0.nmconnection &>/dev/null
		systemctl restart NetworkManager
        else
        	ip link del bond0
        	ip link del br0
        fi

        for i in $(seq 1 $P_I);do
        for j in $(seq 1 $P_J);do
		ip netns exec ns${i}_${j} pkill iperf3
		ip netns exec ns${i}_${j} pkill netserver
                ip netns del ns${i}_${j}
                ip link del ns${i}_${j}_veth0 &>/dev/null
                ip link del ns${i}_${j}_veth1 &>/dev/null
	done
        done

	modprobe -r bonding
}

