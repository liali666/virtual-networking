#!/bin/sh

function assertEquals()
{
	local msg=$1
	local value1=$2
	local value2=$3

	[ -z "$value1" -o -z "$value2" ] && { echo "vlaue can't be null";return 1; }

	if [ "$value1" != "$value2" ];then
		echo $msg
		return 1
	fi
	return 0
}

function assertGreater()
{
	local msg=$1
	local value1=$2
	local value2=$3

	[ -z "$value1" -o -z "$value2" ] && { echo "vlaue can't be null";return 1; }

	if [ $value1 -le $value2 ];then
		echo $msg
		return 1
	fi
	return 0
}
