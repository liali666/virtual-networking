#!/bin/bash

echo $TOPO | grep -q ovs && {
	echo "SKIP"
	exit 254
}

ip link add veth0 type veth peer veth0_p
ip link add veth1 type veth peer veth1_p
ip link add veth2 type veth peer veth2_p
ip link add name br0 type bridge
ip link set br0 up
ip link set veth0_p master br0
ip link set veth1_p master br0
ip link set veth2_p master br0
ip link set veth0_p up
ip link set veth1_p up
ip link set veth2_p up
ip netns add ns1
ip link set veth2 netns ns1 up


nmcli connection add con-name bond0 type bond ifname bond0 bond.options "mode=balance-xor,balance-slb=1,xmit_hash_policy=vlan+srcmac" \
        ipv4.method manual ipv4.address 199.19.19.19/24 ipv6.method manual ipv6.address 2019:19::19/64
nmcli connection add con-name veth0 type ethernet ifname veth0 master bond0
nmcli connection add con-name veth1 type ethernet ifname veth1 master bond0
nmcli connection up veth0
nmcli connection up veth1
nmcli connection up bond0
sleep 10

# start tcpdump
tcpdump -i veth1 -enn -Q out -w failover.cap &
sleep 3

# simulate failover
ip link set veth0 down
sleep 30
pkill tcpdump
sleep 5

tcpdump -r 1.cap -enn 2>/dev/null > output
#199.19.19.19/24 ipv6.method manual ipv6.address 2019:19::19
tcpdump -r failover.cap -enn 2>/dev/null | grep "ethertype ARP.*199.19.19.19" || {
	EXIT_CODE=1
	echo "BUG: gARP was not sent for bond ip 199.19.19.19"
}
tcpdump -r failover.cap -enn 2>/dev/null | grep "ICMP6, neighbor advertisement, tgt is 2019:19::19," || {
	EXIT_CODE=1
	echo "BUG: ipv6 gARP was not sent for bond ip6 2019:19::19"
}

nmcli con del bond0
nmcli con del veth0
nmcli con del veth1
ip netns del ns1
ip link del veth0
ip link del veth1 &> /dev/null
ip link del br0

exit $EXIT_CODE
