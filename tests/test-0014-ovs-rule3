#!/bin/sh

# https://docs.openvswitch.org/en/latest/topics/bonding/#slb-bondin
# Suppose that a MAC+VLAN moves to an SLB bond from another port (e.g. when a VM is migrated from this hypervisor to a different one). 
# Without additional special handling, Open vSwitch will not notice until the MAC learning entry expires, up to 60 seconds later as a consequence of rule #2.
#
# Open vSwitch avoids a 60-second delay by listening for gratuitous ARPs, which VMs commonly emit upon migration. 
# As an exception to rule #2, a gratuitous ARP received on an SLB bond is not dropped and updates the MAC learning table in the usual way. 
# (If a move does not trigger a gratuitous ARP, or if the gratuitous ARP is lost in the network, then a 60-second delay still occurs.)

CASE_DIR=$(dirname $(readlink -f $BASH_SOURCE))
LIB_DIR=${CASE_DIR%/*}
TOPO=${TOPO:-create-virtual-topo.sh}
source $LIB_DIR/$TOPO
source $LIB_DIR/bin/tools.sh

TIME=${TIME:-30}
LOG_FILE=${LOG_FILE:-"./icmp.log"}
EXIT_CODE=0

dmesg -C

# do some operation before testing
TOGGLE_CARRIER=${TOGGLE_CARRIER:-"no"}
RESTART_NM=${RESTART_NM:-"no"}
DEL_CONN=${DEL_CONN:-"no"}
[[ "$RESTART_NM" == "yes" ]] && restart_networkmanager
[[ "$DEL_CONN" == "yes" ]] && delete_connection
[[ "$TOGGLE_CARRIER" == "yes" ]] && toggle_carrier

trap "kill -9 0" 2

[ -f "$LOG_FILE" ] || touch $LOG_FILE
$YUM_INSTALL python3 &>/dev/null || { echo "install python3 failed"; }
$YUM_INSTALL python3-scapy &>/dev/null || pip3 install scapy &>/dev/null || { echo "install scapy failed"; }

slave1=sw1-bond0-veth0
slave2=sw2-bond0-veth0
multicast_ip=224.10.10.11


# ping from vlan1-2 
ip netns exec vlan1-2 ping -c 10 $ip8

# simulate vlan1-2 migration
ip link set vlan1-2-veth1 down

# forge vlan1-2's gratuitous arp in vlan1-4
cat <<EOF >sendm.py
#!/bin/python3
from scapy.all import *
import os, sys

for iface in os.listdir('/sys/class/net/'):
    if iface != 'lo' and iface != 'bonding_masters':
        break
srcmac = "$mac2"
arp = Ether(dst="ff:ff:ff:ff:ff:ff",src=srcmac)/ARP(pdst="$ip2",psrc="$ip2",hwsrc=srcmac)
#arp.show()
sendp(arp, iface=iface)
arp = Ether(dst="ff:ff:ff:ff:ff:ff",src=srcmac)/ARP(pdst="$ip2",psrc="$ip2",hwsrc=srcmac,op="is-at")
#arp.show()
sendp(arp, iface=iface)
EOF

if scapy -h > /dev/null; then
        while :;do
                ip netns exec vlan1-4 python3 sendm.py &>/dev/null || { echo "Warning from scapy"; }
                sleep 1;
        done &
        pid1=$!
else
        echo "ERR: please install scapy"
        exit 1
fi

if ! timeout 20s tcpdump -i vlan1-1-veth1 -c 1 -Q out arp and arp src $ip2 and arp dst $ip2 and ether src $mac2 and ether dst ff:ff:ff:ff:ff:ff  &>/dev/null;then
	EXIT_CODE=1
	echo "BUG: bond dropped gratuitous arp"
fi

# check if fdb entry could be updated
if echo $TOPO | grep -q ovs;then
	for i in `seq 1 30`;do
		fdb_info=$(ovs-appctl fdb/show br0 | grep -E "[[:space:]]+?[0-9]+[[:space:]]+?1[[:space:]]+?${mac2}")
		if [ -n "$fdb_info" ];then
			if ! echo $fdb_info | grep -E "[[:space:]]+?$bond0_ofport[[:space:]]+?1[[:space:]]+?${mac2}";then
				echo "fdb was not updated properly. $fdb_info"
				EXIT_CODE=1
				break
			fi
		fi
	        sleep 1
	done
else
	for i in `seq 1 30`;do
		
		fdb_info=$(bridge fdb show | grep "${mac2}.*vlan 1.*br0")
		if [ -n "$fdb_info" ];then
			if ! echo $fdb_info | grep bond0;then
				echo "fdb was not updated properly. $fdb_info"
				EXIT_CODE=1
				break
			fi
		fi
	        sleep 1
	done
fi

kill -9 $pid1 &>/dev/null

# ping from vlan3-2 
ip netns exec vlan3-2 ping -c 10 $ip10

# simulate vlan3-2 migration
ip link set vlan3-2-veth1 down

# forge vlan3-2's gratuitous arp in vlan3-4
cat <<EOF >sendm.py
#!/bin/python3
from scapy.all import *
import os, sys

for iface in os.listdir('/sys/class/net/'):
    if iface != 'lo' and iface != 'bonding_masters':
        break
srcmac = "$mac5"
arp = Ether(dst="ff:ff:ff:ff:ff:ff",src=srcmac)/ARP(pdst="$ip5",psrc="$ip5",hwsrc=srcmac)
#arp.show()
sendp(arp, iface=iface)
arp = Ether(dst="ff:ff:ff:ff:ff:ff",src=srcmac)/ARP(pdst="$ip5",psrc="$ip5",hwsrc=srcmac,op="is-at")
#arp.show()
sendp(arp, iface=iface)
EOF

if scapy -h > /dev/null; then
        while :;do
                ip netns exec vlan3-4 python3 sendm.py &>/dev/null || { echo "Warning from scapy"; }
                sleep 1;
        done &
        pid1=$!
else
        echo "ERR: please install scapy"
        exit 1
fi

if ! timeout 20s tcpdump -i vlan3-1-veth1 -c 1 -Q out arp and arp src $ip5 and arp dst $ip5 and ether src $mac5 and ether dst ff:ff:ff:ff:ff:ff  &>/dev/null;then
	EXIT_CODE=1
	echo "BUG: bond dropped gratuitous arp in vlan 3"
fi

# check if fdb entry could be updated
if echo $TOPO | grep -q ovs;then
	for i in `seq 1 30`;do
		if echo $TOPO | grep -q "vlan-over-bond";then
			fdb_info=$(ovs-appctl fdb/show br1 | grep -E "[[:space:]]+?[0-9]+[[:space:]]+?3[[:space:]]+?${mac5}")
			if [ -n "$fdb_info" ];then
				if ! echo $fdb_info | grep -E "[[:space:]]+?$bond0_3_ofport[[:space:]]+?3[[:space:]]+?${mac5}";then
					echo "fdb was not updated properly. $fdb_info"
					EXIT_CODE=1
					break
				fi
			fi
		else
			fdb_info=$(ovs-appctl fdb/show br0 | grep -E "[[:space:]]+?[0-9]+[[:space:]]+?3[[:space:]]+?${mac5}")
			if [ -n "$fdb_info" ];then
				if ! echo $fdb_info | grep -E "[[:space:]]+?$bond0_ofport[[:space:]]+?3[[:space:]]+?${mac5}";then
					echo "fdb was not updated properly. $fdb_info"
					EXIT_CODE=1
					break
				fi
			fi
		fi
	        sleep 1
	done
else
	for i in `seq 1 30`;do
		if echo $TOPO | grep -q "vlan-over-bond";then
			fdb_info=$(bridge fdb show | grep "${mac5}.*vlan 3.*br1")
		else
			fdb_info=$(bridge fdb show | grep "${mac5}.*vlan 3.*br0")
		fi
		if [ -n "$fdb_info" ];then
			if ! echo $fdb_info | grep bond0;then
				echo "fdb was not updated properly. $fdb_info"
				EXIT_CODE=1
				break
			fi
		fi
	        sleep 1
	done
fi

kill -9 $pid1 &>/dev/null

# no need to chack packet loss when above step failed
if [ $EXIT_CODE -eq 0 ];then
	# vm migration
	ip netns exec vlan1-4 ip addr flush vlan1-4-veth1
	ip netns exec vlan1-4 ip link set vlan1-4-veth1 address $mac2
	ip netns exec vlan1-4 ip addr add ${ip2}/24 dev vlan1-4-veth1

	echo "******** noloss Pinging from vlan1-1 to vlan1-4 ********"
	if ip netns exec vlan1-1 nolossping "${TIME}" 1 2 $ip2;then
	        echo -e "        ******** Succeeded ********\n"
	else
	        echo -e "        ******** Failed ********\n"
	        EXIT_CODE=1
	fi

	echo "******** noloss Pinging from vlan1-5 to vlan1-4 ********"
	if ip netns exec vlan1-5 nolossping "${TIME}" 1 2 $ip2;then
	        echo -e "        ******** Succeeded ********\n"
	else
	        echo -e "        ******** Failed ********\n"
	        EXIT_CODE=1
	fi

	# vm migration
	ip netns exec vlan3-4 ip addr flush vlan3-4-veth1
	ip netns exec vlan3-4 ip link set vlan3-4-veth1 address $mac5
	ip netns exec vlan3-4 ip addr add ${ip5}/24 dev vlan3-4-veth1

	echo "******** noloss Pinging from vlan3-1 to vlan3-4 ********"
	if ip netns exec vlan3-1 nolossping "${TIME}" 1 2 $ip5;then
	        echo -e "        ******** Succeeded ********\n"
	else
	        echo -e "        ******** Failed ********\n"
	        EXIT_CODE=1
	fi

	echo "******** noloss Pinging from vlan3-5 to vlan3-4 ********"
	if ip netns exec vlan3-5 nolossping "${TIME}" 1 2 $ip5;then
	        echo -e "        ******** Succeeded ********\n"
	else
	        echo -e "        ******** Failed ********\n"
	        EXIT_CODE=1
	fi
fi

rm -rf $LOG_FILE &>/dev/null

cleanup_topo

dmesg | grep -i "Call Trace" && { echo "Found Call Trace"; EXIT_CODE=1; }

exit $EXIT_CODE
