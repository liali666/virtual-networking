#!/bin/sh
CASE_DIR=$(dirname $(readlink -f $BASH_SOURCE))
LIB_DIR=${CASE_DIR%/*}
TOPO=${TOPO:-create-virtual-topo.sh}

source $LIB_DIR/$TOPO
source $LIB_DIR/bin/tools.sh

PING_COUNT=${PING_COUNT:-60}
THRESHOLD=${THRESHOLD:-10}
LOG_FILE=${LOG_FILE:-"./icmp.log"}
EXIT_CODE=0

dmesg -C

# do some operation before testing
TOGGLE_CARRIER=${TOGGLE_CARRIER:-"no"}
RESTART_NM=${RESTART_NM:-"no"}
DEL_CONN=${DEL_CONN:-"no"}
[[ "$RESTART_NM" == "yes" ]] && restart_networkmanager
[[ "$DEL_CONN" == "yes" ]] && delete_connection
[[ "$TOGGLE_CARRIER" == "yes" ]] && toggle_carrier

[ -f "$LOG_FILE" ] || touch $LOG_FILE

# function for change active slave
function change_active_slave(){
	active_slave=$(nft list table netdev nm-mlag-bond0 | grep "tx-redirect-igmp-reports" -A 4 | grep -P -o "(?<=fwd to \").*(?=\")"| head -n1)
	if [ -z "$active_slave" ];then
		echo "BUG: can't find active slave"
	elif [ "$active_slave" == "sw1-bond0-veth0" ];then
	        backup_slave=sw2-bond0-veth0
	else
	        backup_slave=sw1-bond0-veth0
	fi

	#active_slave=$(cat /sys/class/net/bond0/bonding/active_slave)
	#if [ "$active_slave" == "sw1-bond0-veth0" ];then
	#	backup_slave=sw2-bond0-veth0
	#else
	#	backup_slave=sw1-bond0-veth0
	#fi
	
	# no active slave when mode is 2
	if [ -z "$active_slave" -o -z "$backup_slave" ];then
		echo "no active slave"
		ip link set sw1-bond0-veth0 down
		sleep 10
		ip link set sw1-bond0-veth0 up
		sleep 10
		ip link set sw2-bond0-veth0 down
		sleep 10
		ip link set sw2-bond0-veth0 up
	else
		echo "active slave is: $active_slave"
		echo "backup slave is: $backup_slave"

		ip link set $active_slave down
		sleep 10
		ip link set $active_slave up
	fi
}

echo "******** test ip6 failver"
for i in vlan1-1%$ip6_7 vlan1-1%$ip6_8 vlan1-2%$ip6_7 vlan1-2%$ip6_8 vlan1-4%$ip6_1 vlan1-4%$ip6_2 vlan1-5%$ip6_1 vlan1-5%$ip6_2;do
        src=$(echo $i|awk -F"%" '{print $1}')
        dst=$(echo $i|awk -F"%" '{print $2}')
        #echo sw1-bond0-veth0 > /sys/class/net/bond0/bonding/active_slave

        # start ping
        echo "**** ping from $src to $dst ****" 
        ip netns exec $src ping6 -c $PING_COUNT $dst &>$LOG_FILE &
        sleep 5

        # change active slave
        change_active_slave
        wait

        # check packet loss
        cat $LOG_FILE
        rate=`grep -o "[0-9.]*% packet loss" $LOG_FILE | awk -F"%" '{print $1}' | awk -F"." '{print $1}'`
        if [ $rate -le $THRESHOLD ]; then
            echo "Failover: $rate% packet loss , PASS"
        else
            echo "Failover: $rate% packet loss , FAIL"
            EXIT_CODE=1
        fi

        echo ""
done

echo "******** test ip4 failver"
for i in vlan1-1:$ip7 vlan1-1:$ip8 vlan1-2:$ip7 vlan1-2:$ip8 vlan1-4:$ip1 vlan1-4:$ip2 vlan1-5:$ip1 vlan1-5:$ip2;do
	src=$(echo $i|awk -F":" '{print $1}')
	dst=$(echo $i|awk -F":" '{print $2}')
	#echo sw1-bond0-veth0 > /sys/class/net/bond0/bonding/active_slave

	# start ping
	echo "**** ping from $src to $dst ****" 
	ip netns exec $src ping -c $PING_COUNT $dst &>$LOG_FILE &
	sleep 5

	# change active slave
	change_active_slave
	wait
	
	# check packet loss
	cat $LOG_FILE
        rate=`grep -o "[0-9.]*% packet loss" $LOG_FILE | awk -F"%" '{print $1}' | awk -F"." '{print $1}'`
        if [ $rate -le $THRESHOLD ]; then
            echo "Failover: $rate% packet loss , PASS"
        else
            echo "Failover: $rate% packet loss , FAIL"
            EXIT_CODE=1
        fi

	echo ""
done

rm -rf $LOG_FILE &>/dev/null

cleanup_topo

dmesg | grep -i "Call Trace" && { echo "Found Call Trace"; EXIT_CODE=1; }

exit $EXIT_CODE
