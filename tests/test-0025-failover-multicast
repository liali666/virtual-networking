#!/bin/sh

# 
# check multicast loss after failover

CASE_DIR=$(dirname $(readlink -f $BASH_SOURCE))
LIB_DIR=${CASE_DIR%/*}
TOPO=${TOPO:-create-virtual-topo.sh}
TOPO=create-virtual-topo-igmp-snooping-on.sh
source $LIB_DIR/$TOPO
source $LIB_DIR/bin/tools.sh

TIME=${TIME:-30}
LOG_FILE=${LOG_FILE:-"./icmp.log"}
EXIT_CODE=0

dmesg -C

# do some operation before testing
TOGGLE_CARRIER=${TOGGLE_CARRIER:-"no"}
RESTART_NM=${RESTART_NM:-"no"}
DEL_CONN=${DEL_CONN:-"no"}
[[ "$RESTART_NM" == "yes" ]] && restart_networkmanager
[[ "$DEL_CONN" == "yes" ]] && delete_connection
[[ "$TOGGLE_CARRIER" == "yes" ]] && toggle_carrier

trap "kill -9 0" 2

[ -f "$LOG_FILE" ] || touch $LOG_FILE
$YUM_INSTALL python3 &>/dev/null || { echo "install python3 failed"; }
$YUM_INSTALL python3-scapy &>/dev/null || pip3 install scapy &>/dev/null || { echo "install scapy failed"; }

#setup_topo

slave1=sw1-bond0-veth0
slave2=sw2-bond0-veth0
multicast_ip=224.10.10.10

echo -e "\n-- only vlan1-1 join a group"

# resetup topo 
#setup_topo

join_group()
{
        # send igmp join from vlan1-1
        cat <<- EOF >sendm.py
	#!/bin/python3
	from scapy.all import *
	from scapy.contrib.igmpv3 import IGMPv3,IGMPv3mq,IGMP,IGMPv3gr
	from scapy.contrib.igmpv3 import IGMPv3mr
	import os, sys
	
	for iface in os.listdir('/sys/class/net/'):
	    if iface != 'lo' and iface != 'bonding_masters':
	        break
	srcmac = open("/sys/class/net/" + iface + "/address", "r").read()
	
	p_join = Ether(dst='01:00:5e:0a:0a:0a', src=srcmac) / IP(src="$ip1", dst="$multicast_ip", tos=0xc0) /IGMPv3() /IGMPv3mr(numgrp=1) /IGMPv3gr(rtype=4, maddr="$multicast_ip")
	#p_join.show()
	sendp(p_join,iface=iface)
	EOF

	ip netns exec vlan1-1 ip addr add $multicast_ip dev vlan1-1-veth0 autojoin
        if scapy -h > /dev/null; then
                ip netns exec vlan1-1 python3 sendm.py &>/dev/null || { echo "Warning from scapy"; }
        else
                echo "ERR: please install scapy"
                exit 1
        fi
}

check_multicast()
{
	# workaround, send multicast from vlan1-4
	ip netns exec vlan1-4 ping $multicast_ip -I vlan1-4-veth1 -c 60 &>/dev/null || echo "nothing"
	ip netns exec vlan1-1 tcpdump -i vlan1-1-veth0 ether src $mac7 and src host $ip7 and dst host $multicast_ip -w 1.cap &>/dev/null &
	ip netns exec vlan1-2 tcpdump -i vlan1-2-veth0 ether src $mac7 and src host $ip7 and dst host $multicast_ip -w 2.cap &>/dev/null &
	ip netns exec vlan1-3 tcpdump -i vlan1-3-veth0 ether src $mac7 and src host $ip7 and dst host $multicast_ip -w 3.cap &>/dev/null &
	sleep 5
	
	sleep 10
	
	# send multicast from vlan1-4
	ip netns exec vlan1-4 ping $multicast_ip -I vlan1-4-veth1 -c 10 &>/dev/null || echo "nothing"
	
	pkill tcpdump &>/dev/null || echo "Waring from tcpdump"
	sleep 2
	
	echo "-- check result"
	tcpdump -r 1.cap
	tcpdump -r 2.cap
	tcpdump -r 3.cap
	pkt_count=$(tcpdump -r 1.cap | wc -l)
	if [ "$pkt_count" != "10" ];then
	        EXIT_CODE=1
	        echo "BUG: vlan1-1 received $pkt_count multicast packets"
	fi
	pkt_count=$(tcpdump -r 2.cap | wc -l)
	if [ "$pkt_count" != "0" ];then
	        EXIT_CODE=1
	        echo "BUG: vlan1-2 received $pkt_count multicast packets"
	fi
	pkt_count=$(tcpdump -r 3.cap | wc -l)
	if [ "$pkt_count" != "0" ];then
	        EXIT_CODE=1
	        echo "BUG: vlan1-3 received $pkt_count multicast packets"
	fi
}

check_resend_igmp()
{

	#active_slave=$(cat /sys/class/net/bond0/bonding/active_slave)
	active_slave=$(nft list table netdev nm-mlag-bond0 | grep "tx-redirect-igmp-reports" -A 4 | grep -P -o "(?<=fwd to \").*(?=\")"| head -n1)
	if [ -z "$active_slave" ];then
		echo "BUG: can't find active slave"
	elif [ "$active_slave" == "sw1-bond0-veth0" ];then
	        backup_slave=sw2-bond0-veth0
	else
	        backup_slave=sw1-bond0-veth0
	fi
	if [ -n "$backup_slave" ];then
		#tcpdump -i $backup_slave -Q out dst ff02::16 and ip6[48]=0x83 -w igmp.cap & 
		tcpdump -i $backup_slave -Q out igmp and igmp[0]=0x22 and dst $multicast_ip -w igmp.cap & 
		sleep 3
		ip link set $active_slave down
		sleep 10
		pkill tcpdump &>/dev/null || echo "Waring from tcpdump"
		sleep 2
		pkt_count=$(tcpdump -r igmp.cap | wc -l)
		if [ $pkt_count -eq 0 ];then
			EXIT_CODE=1
			echo "BUG: didn't resend igmp join"
		fi
		ip link set $active_slave up
	fi
}

check_igmp_steer()
{
	for mac in {0..9};do
		# resetup topo 
		veth_mac=$(printf "00:22:22:23:%02x:01" $mac)
		echo -e "\n-- vlan1-3 join a group with mac: $veth_mac"
		ip netns exec vlan1-3 ip link set vlan1-3-veth0 address $veth_mac
	
		# start ping from innter vms and will check pacekt loss at the end
		ip netns exec vlan1-4 nolossping 60 1 -1 $ip3 &
		pid0=$!
		ip netns exec vlan1-5 nolossping 60 1 -1 $ip3 &
		pid1=$!
		
		#active_slave=$(cat /sys/class/net/bond0/bonding/active_slave)
		active_slave=$(nft list table netdev nm-mlag-bond0 | grep "tx-redirect-igmp-reports" -A 4 | grep -P -o "(?<=fwd to \").*(?=\")"| head -n1)
		if [ -z "$active_slave" ];then
			echo "BUG: can't find active slave"
		elif [ "$active_slave" == "sw1-bond0-veth0" ];then
		        backup_slave=sw2-bond0-veth0
		else
		        backup_slave=sw1-bond0-veth0
		fi
		if [ -n "$active_slave" ];then
			tcpdump -i $active_slave -Q out igmp and igmp[0]=0x22 and dst $multicast_ip -w igmp1.cap &
			tcpdump -i $backup_slave -Q out igmp and igmp[0]=0x22 and dst $multicast_ip -w igmp2.cap & 
			sleep 3
		fi
	
		ip netns exec vlan1-3 ip addr add $multicast_ip dev vlan1-3-veth0 autojoin
		# send igmp join from vlan1-3
		if scapy -h > /dev/null; then
		        ip netns exec vlan1-3 python3 sendm.py &>/dev/null || { echo "Warning from scapy"; }
		else
		        echo "ERR: please install scapy"
		        exit 1
		fi
		
		sleep 5
		
		pkill tcpdump &>/dev/null || echo "Waring from tcpdump"
		sleep 2
		pkt_count=$(tcpdump -r igmp2.cap | wc -l)
		if [ "$pkt_count" != "0" ];then
			EXIT_CODE=1
			echo "BUG: backup slave send igmp join packets out"
			tcpdump -r igmp2.cap -eenn
		fi

		wait $pid0 || { EXIT_CODE=1; echo "nolossping 0 failed"; }
		wait $pid1 || { EXIT_CODE=1; echo "nolossping 1 failed"; }
		
		[[ $EXIT_CODE == 1 ]] && break
	done
}

join_group
check_multicast
echo "---- bring down sw1-bond0-veth0"
ip link set sw1-bond0-veth0 down
check_multicast
echo "---- bring up sw1-bond0-veth0"
ip link set sw1-bond0-veth0 up
check_multicast
echo "---- bring down sw2-bond0-veth0"
ip link set sw2-bond0-veth0 down
check_multicast
echo "---- bring up sw2-bond0-veth0"
ip link set sw2-bond0-veth0 up
sleep 5
check_resend_igmp
check_igmp_steer

rm -rf $LOG_FILE &>/dev/null

cleanup_topo

dmesg | grep -i "Call Trace" && { echo "Found Call Trace"; EXIT_CODE=1; }

exit $EXIT_CODE

