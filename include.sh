#!/bin/bash

[ -z "$YUM_INSTALL" ] && {
	pkg_mgr_cmd="yum"
	if ! which yum &> /dev/null; then
	        pkg_mgr_cmd='dnf'
	fi
	if stat /run/ostree-booted > /dev/null 2>&1; then
	        YUM_INSTALL="rpm-ostree -A --assumeyes --idempotent --allow-inactive install"
	else
	        YUM_INSTALL="$pkg_mgr_cmd -y install"
	fi
}
